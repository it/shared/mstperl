
=begin
Begin-Doc
Name: MST::HostGroup
Type: module
Description: NIS hostgroup maintenance
End-Doc
=cut

package MST::HostGroup;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw(
    HostGroup_Exists
    HostGroup_ExistsPrefix
    HostGroup_ExistsMulti
    HostGroup_List
    HostGroup_List_WithPrefix
    HostGroup_List_WithPrefixMulti
    HostGroup_List_Changed
    HostGroup_List_Changed_WithPrefix
    HostGroup_List_Changed_WithPrefixMulti
    HostGroup_Create
    HostGroup_Delete
    HostGroup_AddMemberComputersByUserID
    HostGroup_AddMemberComputersByDN
    HostGroup_DeleteMemberComputersByUserID
    HostGroup_DeleteMemberComputersByDN
    HostGroup_MemberComputers
    HostGroup_MemberComputersMulti
    HostGroup_MemberComputersDN
    HostGroup_MemberComputersDNMulti
    HostGroup_MemberOf
    HostGroup_MemberOfMulti
    HostGroup_GetProtectedMulti
    HostGroup_GetDescription
    HostGroup_GetDescriptionMulti
    HostGroup_SetDescription
    HostGroup_GetNotes
    HostGroup_GetNotesMulti
    HostGroup_SetNotes
);

use Local::UsageLogger;
use Local::SimpleRPC;
use Local::CurrentUser;
use Local::AuthSrv;

use MST::Env;

our $ADMIN_RPC;

BEGIN {
    &LogAPIUsage();
}

# Begin-Doc
# Name: _NIS_syslog
# Type: function
# Access: internal
# Description: wrapper around syslog function to allow it to be ignored on windows
# End-Doc
sub _NIS_syslog {
    my @args = @_;

    # Allow code to function on windows
    eval "use Sys::Syslog";
    eval { syslog(@args); };
}

# Begin-Doc
# Name: HostGroup_SimpleRPC
# Type: function
# Description: Helper routine to set up RPC connection for computer apps
# Syntax: $rpc = &HostGroup_SimpleRPC()
# Access: internal
# End-Doc
sub HostGroup_SimpleRPC {

    if ($ADMIN_RPC) {
        return $ADMIN_RPC;
    }

    my $rpchost;
    my $env = &MST_Env();

    if ( $env eq "dev" ) {
        $rpchost = "mstgrpmaint.apps-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "mstgrpmaint.apps-test.mst.edu";
    }
    else {
        $rpchost = "mstgrpmaint.apps.mst.edu";
    }

    my $curuser = &Local_CurrentUser();

    my $token = &AuthSrv_Fetch( "instance" => "api-token-mstgrpmaint" );
    if ($token) {
        $ADMIN_RPC = new Local::SimpleRPC::Client(
            base_url => "https://${rpchost}/auth-api-bin/latest/HostGroup",
            timeout  => 120,
            retries  => 2,
            version  => 2,
            user     => $curuser,
            password => $token
        );
    }
    else {
        die "Couldn't look up token";
    }

    return $ADMIN_RPC;
}

# Begin-Doc
# Name: HostGroup_Exists
# Type: function
# Description: returns true if hostgroup exists
# Syntax: $res = &HostGroup_Exists($group)
# End-Doc
sub HostGroup_Exists {
    my $group = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->Exists( group => $group, actor => $ENV{REMOTE_USER} );

    return int( $info->{$group} );
}

# Begin-Doc
# Name: HostGroup_ExistsPrefix
# Type: function
# Description: returns true if hostgroup or any prefix of that hostgroup exist
# Syntax: $res = &HostGroup_ExistsPrefix($group)
# End-Doc
sub HostGroup_ExistsPrefix {
    my $group = shift;

    my @tmp    = split( '-', $group );
    my @groups = ();
    for ( my $i = 0; $i <= $#tmp; $i++ ) {
        my $tmpgrp = join( "-", @tmp[ 0 .. $i ] );
        push( @groups, $tmpgrp );
    }

    my $res = &HostGroup_ExistsMulti(@groups);
    if ( !$res ) {
        return 0;
    }

    foreach my $grp (@groups) {
        if ( $res->{$grp} ) {
            return 1;
        }
    }
    return 0;
}

# Begin-Doc
# Name: HostGroup_ExistsMulti
# Type: function
# Description: returns true if hostgroups exist
# Syntax: $res = &HostGroup_ExistsMulti($group, $group, ...)
# Returns: hash keyed on group, value is 1 if group exists
# End-Doc
sub HostGroup_ExistsMulti {
    my @groups = @_;
    my @args;

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->Exists( @args, actor => $ENV{REMOTE_USER} );

    return $info;
}

# Begin-Doc
# Name: HostGroup_List
# Type: function
# Description: Returns array of hostgroups
# Syntax: @groups = &HostGroup_List
# End-Doc
sub HostGroup_List {
    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->List( actor => $ENV{REMOTE_USER} );

    return @$info;
}

# Begin-Doc
# Name: HostGroup_List_WithPrefix
# Type: function
# Description: returns arrays of hostgroup with prefix
# Syntax: $res = &HostGroup_List_WithPrefix($prefix)
# End-Doc
sub HostGroup_List_WithPrefix {
    my $prefix = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->List( prefix => $prefix, actor => $ENV{REMOTE_USER} );

    return @$info;
}

# Begin-Doc
# Name: HostGroup_List_WithPrefixMulti
# Type: function
# Description: returns arrays of hostgroup with prefixes (prefix1 OR prefix2)
# Syntax: $res = &HostGroup_List_WithPrefixMulti($prefix, $prefix, ...)
# End-Doc
sub HostGroup_List_WithPrefixMulti {
    my @prefixes = @_;
    my @args;

    foreach my $prefix (@prefixes) {
        push( @args, "prefix", $prefix );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->List( @args, actor => $ENV{REMOTE_USER} );

    return @$info;
}

# Begin-Doc
# Name: HostGroup_List_Changed
# Type: function
# Description: Returns array of recently changed hostgroups
# Syntax: @groups = &HostGroup_List_Changed
# End-Doc
sub HostGroup_List_Changed {
    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->ListChanged( actor => $ENV{REMOTE_USER} );

    return @$info;
}

# Begin-Doc
# Name: HostGroup_List_Changed_WithPrefix
# Type: function
# Description: Returns array of recently changed hostgroups with prefix
# Syntax: @groups = &HostGroup_List_Changed_WithPrefix($prefix)
# End-Doc
sub HostGroup_List_Changed_WithPrefix {
    my $prefix = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->ListChanged( prefix => $prefix, actor => $ENV{REMOTE_USER} );

    return @$info;
}

# Begin-Doc
# Name: HostGroup_List_Changed_WithPrefixMulti
# Type: function
# Description: Returns array of recently changed hostgroups with prefixes (prefix1 OR prefix2)
# Syntax: @groups = &HostGroup_List_Changed_WithPrefixMulti($prefix, $prefix, ...)
# End-Doc
sub HostGroup_List_Changed_WithPrefixMulti {
    my @prefixes = @_;
    my @args;

    foreach my $prefix (@prefixes) {
        push( @args, "prefix", $prefix );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->ListChanged( @args, actor => $ENV{REMOTE_USER} );

    return @$info;
}

# Begin-Doc
# Name: HostGroup_Create
# Type: function
# Description: Creates a hostgroup. The name should start with 'hg-'
# Syntax: &HostGroup_Create($group, $tier)
# Returns: zero or undef on success, error msg on failure
# End-Doc
sub HostGroup_Create {
    my $group = shift;
    my $tier  = shift || "2";

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info", "HostGroup_Create($group in tier $tier) by " . $ENV{REMOTE_USER} . " from host " . $rh );

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->Create( group => $group, tier => $tier, actor => $ENV{REMOTE_USER} ); };

    return $@;
}

# Begin-Doc
# Name: HostGroup_Delete
# Type: function
# Description: Deletes a hostgroup and all members
# Syntax: &HostGroup_Delete($group)
# Returns: zero or undef on success, error msg on failure
# End-Doc
sub HostGroup_Delete {
    my ($group) = @_;

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info", "HostGroup_Delete($group) by " . $ENV{REMOTE_USER} . " from host " . $rh );

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->Delete( group => $group, actor => $ENV{REMOTE_USER} ); };

    return $@;
}

# Begin-Doc
# Name: HostGroup_AddMemberComputersByUserID
# Type: function
# Description: Adds a computer to a hostgroup by userid (computer userid format: COMPUTERNAME$)
# Syntax: &HostGroup_AddMemberComputersByUserID($group, @computers)
# Comments: Note the argument ordering.
# End-Doc
sub HostGroup_AddMemberComputersByUserID {
    my ( $group, @computers ) = @_;

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "HostGroup_AddMemberComputersByUserID("
            . join( ",", @computers )
            . " to $group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    @computers = map {lc} @computers;
    $group     = lc $group;

    my @args;
    push( @args, "group", $group );
    foreach my $computer (@computers) {
        push( @args, "computer", $computer );
    }

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->AddMemberComputersByUserID( @args, actor => $ENV{REMOTE_USER} ); };
    return $@;
}

# Begin-Doc
# Name: HostGroup_AddMemberComputersByDN
# Type: function
# Description: Adds a computer to a hostgroup by distinguished name
# Syntax: &HostGroup_AddMemberComputersByDN($group, @dn)
# Comments: Note the argument ordering.
# End-Doc
sub HostGroup_AddMemberComputersByDN {
    my ( $group, @dn ) = @_;

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "HostGroup_AddMemberComputersByDN("
            . join( ",", @dn )
            . " to $group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    @dn    = map {lc} @dn;
    $group = lc $group;

    my @args;
    push( @args, "group", $group );
    foreach my $dn (@dn) {
        push( @args, "dn", $dn );
    }

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->AddMemberComputersByDN( @args, actor => $ENV{REMOTE_USER} ); };
    return $@;
}

# Begin-Doc
# Name: HostGroup_DeleteMemberComputersByUserID
# Type: function
# Description: Deletes one or more computer from a hostgroup by userid (computer userid format: COMPUTERNAME$)
# Syntax: &HostGroup_DeleteMemberComputersByUserID($group, @computers)
# Comments: Note the argument ordering.
# End-Doc
sub HostGroup_DeleteMemberComputersByUserID {
    my ( $group, @computers ) = @_;

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "HostGroup_DeleteMemberComputersByUserID("
            . join( ",", @computers )
            . " from $group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    @computers = map {lc} @computers;
    $group     = lc $group;

    my @args;
    push( @args, "group", $group );
    foreach my $computer (@computers) {
        push( @args, "computer", $computer );
    }

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->DeleteMemberComputersByUserID( @args, actor => $ENV{REMOTE_USER} ); };
    return $@;
}

# Begin-Doc
# Name: HostGroup_DeleteMemberComputersByDN
# Type: function
# Description: Deletes one or more computer from a hostgroup by distinguished name
# Syntax: &HostGroup_DeleteMemberComputersByDN($group, @dn)
# Comments: Note the argument ordering.
# End-Doc
sub HostGroup_DeleteMemberComputersByDN {
    my ( $group, @dn ) = @_;

    my $rh = $ENV{HTTP_X_FORWARDED_FOR} || $ENV{REMOTE_HOST} || $ENV{REMOTE_ADDR};
    &_NIS_syslog( "info",
              "HostGroup_DeleteMemberComputersByDN("
            . join( ",", @dn )
            . " from $group) by "
            . $ENV{REMOTE_USER}
            . " from host "
            . $rh );

    @dn    = map {lc} @dn;
    $group = lc $group;

    my @args;
    push( @args, "group", $group );
    foreach my $dn (@dn) {
        push( @args, "dn", $dn );
    }

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->DeleteMemberComputersByDN( @args, actor => $ENV{REMOTE_USER} ); };
    return $@;
}

# Begin-Doc
# Name: HostGroup_MemberComputers
# Type: function
# Description: Returns array of computers that are members of $group
# Syntax: @hosts = &HostGroup_MemberComputers($group)
# End-Doc
sub HostGroup_MemberComputers {
    my $group = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->MemberComputers( group => $group, actor => $ENV{REMOTE_USER} );
    return @{ $info->{$group} };
}

# Begin-Doc
# Name: HostGroup_MemberComputersMulti
# Type: function
# Description: Returns list of computers that are members of each group
# Syntax: %members_by_group = &HostGroup_MemberComputersMulti($group, $group, ...)
# End-Doc
sub HostGroup_MemberComputersMulti {
    my @groups = @_;
    my @args;

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->MemberComputers( @args, actor => $ENV{REMOTE_USER} );

    return %$info;
}

# Begin-Doc
# Name: HostGroup_MemberComputersDN
# Type: function
# Description: Returns array of computer distinguished names that are members of $group
# Syntax: @hosts = &HostGroup_MemberComputers($group)
# End-Doc
sub HostGroup_MemberComputersDN {
    my $group = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->MemberComputersDN( group => $group, actor => $ENV{REMOTE_USER} );
    return @{ $info->{$group} };
}

# Begin-Doc
# Name: HostGroup_MemberComputersDNMulti
# Type: function
# Description: Returns list of computer distinguished names that are members of each group
# Syntax: %members_by_group = &HostGroup_MemberComputersDNMulti($group, $group, ...)
# End-Doc
sub HostGroup_MemberComputersDNMulti {
    my @groups = @_;
    my @args;

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->MemberComputersDN( @args, actor => $ENV{REMOTE_USER} );

    return %$info;
}

# Begin-Doc
# Name: HostGroup_MemberOf
# Type: function
# Description: Returns array of groups that computer $computer is a member of (computer format: COMPUTERNAME$)
# Syntax: @groups = &HostGroup_MemberOf($computer)
# End-Doc
sub HostGroup_MemberOf {
    my $computer = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->MemberOf( computer => $computer, actor => $ENV{REMOTE_USER} );
    return @{ $info->{$computer} };
}

# Begin-Doc
# Name: HostGroup_MemberOfMulti
# Type: function
# Description: Returns array of groups that each computer is a member of (computer format: COMPUTERNAME$)
# Syntax: %groups_by_computer = &HostGroup_MemberOfMulti($computer, $computer, ...)
# Returns: hash keyed on computer, values are arrays of groups
# End-Doc
sub HostGroup_MemberOfMulti {
    my @computers = @_;
    my @args;

    foreach my $computer (@computers) {
        push( @args, "computer", $computer );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->MemberOf( @args, actor => $ENV{REMOTE_USER} );
    return %$info;
}

# Begin-Doc
# Name: HostGroup_GetProtectedMulti
# Type: function
# Description: Returns list of protected status for groups
# Syntax: %desc_by_group = &HostGroup_GetProtectedMulti($group, $group, ...)
# End-Doc
sub HostGroup_GetProtectedMulti {
    my @groups = @_;
    my @args;

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->GetProtected( @args, actor => $ENV{REMOTE_USER} );
    return %$info;
}

# Begin-Doc
# Name: HostGroup_GetDescription
# Type: function
# Description: Returns array of description field for a group
# Syntax: @desc = &HostGroup_GetDescription($group)
# End-Doc
sub HostGroup_GetDescription {
    my $group = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->GetDescription( group => $group, actor => $ENV{REMOTE_USER} );
    return @{ $info->{$group} };
}

# Begin-Doc
# Name: HostGroup_GetDescriptionMulti
# Type: function
# Description: Returns list of description fields for groups
# Syntax: %desc_by_group = &HostGroup_GetDescriptionMulti($group, $group, ...)
# End-Doc
sub HostGroup_GetDescriptionMulti {
    my @groups = @_;
    my @args;

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->GetDescription( @args, actor => $ENV{REMOTE_USER} );
    return %$info;
}

# Begin-Doc
# Name: HostGroup_SetDescription
# Type: function
# Description: Sets a description for a hostgroup
# Syntax: $res = &HostGroup_SetDescription($group, $desc);
# Comments: returns error msg on failure
# End-Doc
sub HostGroup_SetDescription {
    my $group = shift;
    my $desc  = shift;

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->SetDescription( group => $group, description => $desc, actor => $ENV{REMOTE_USER} ) };
    return $@;
}

# Begin-Doc
# Name: HostGroup_GetNotes
# Type: function
# Description: Returns array of notes fields for a group
# Syntax: @notes = &HostGroup_GetNotes($group)
# End-Doc
sub HostGroup_GetNotes {
    my $group = shift;

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->GetNotes( group => $group, actor => $ENV{REMOTE_USER} );
    return @{ $info->{$group} };
}

# Begin-Doc
# Name: HostGroup_GetNotesMulti
# Type: function
# Description: Returns list of notes fields for groups
# Syntax: %desc_by_group = &HostGroup_GetNotesMulti($group, $group, ...)
# End-Doc
sub HostGroup_GetNotesMulti {
    my @groups = @_;
    my @args;

    foreach my $grp (@groups) {
        push( @args, "group", $grp );
    }

    my $rpc  = &HostGroup_SimpleRPC();
    my $info = $rpc->GetNotes( @args, actor => $ENV{REMOTE_USER} );
    return %$info;
}

# Begin-Doc
# Name: HostGroup_SetNotes
# Type: function
# Description: Sets notes for a netgroup
# Syntax: $res = &HostGroup_SetNotes($group, $notes);
# Comments: returns error msg on failure
# End-Doc
sub HostGroup_SetNotes {
    my $group = shift;
    my $notes = shift;

    my $rpc = &HostGroup_SimpleRPC();
    eval { $rpc->SetNotes( group => $group, notes => $notes, actor => $ENV{REMOTE_USER} ) };
    return $@;
}

1;
