
=begin

Begin-Doc
Name: MST::Env
Type: module
Description: MST Environment Detection Routine
Comments: 

End-Doc

=cut

package MST::Env;
require Exporter;
use strict;
use MST::UsageLogger;
use Local::Env;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw( MST_Env MST_Env_Reset );

BEGIN {
    &LogAPIUsage();
}

# Begin-Doc
# Name: MST_Env_Reset
# Type: function
# Description: Resets detected environment name
# Syntax: &MST_Env_Reset()
# Comments: no return
# End-Doc
sub MST_Env_Reset {
    return &Local_Env_Reset();
}

# Begin-Doc
# Name: MST_Env
# Type: function
# Description: Returns detected environment name
# Syntax: $env = &MST_Env()
# Comments: returns one of 'prod', 'test', or 'dev'
# End-Doc
sub MST_Env {
    return &Local_Env();
}

1;
