# Begin-Doc
# Name: MST::MySQLObject
# Type: module
# Description: MST specific wrapper around Local::MySQLObject
# End-Doc
package MST::MySQLObject;
require 5.000;
use parent "Local::MySQLObject";
use Local::Env;
use Local::CurrentUser;
use strict;

# Begin-Doc
# Name: SQL_OpenDatabase
# Type: method
# Description: opens a new database connection
# Syntax: See syntax for Local::MySQLObject with the following addition:
#   In order to connect to environment specific sysdb host use one of: sysd, syst, sysp as the first argument
#   Example: $db->SQL_OpenDatabase("sysp");
#
#   If you want to establish an enviornment aware connection to sysdb use: sys* as first argument
#   Example: $db->SQL_OpenDatabase("sys*");
#
# End-Doc
sub SQL_OpenDatabase {
    my ( $self, $database, %opts ) = @_;
    my $env = &Local_Env();

    my %hosts = (
        sysd => "sysdb-dev.srv.mst.edu",
        syst => "sysdb-test.srv.mst.edu",
        sysp => "sysdb.srv.mst.edu",
    );

    my %suffix = (
        "dev"  => "d",
        "test" => "t",
        "prod" => "p",
    );

    if ( !defined( $opts{host} ) ) {
        if ( $database =~ m/^(.*)\*$/ ) {
            $opts{host} = $hosts{ $1 . $suffix{$env} };
        }
        elsif ( defined( $hosts{$database} ) ) {
            $opts{host} = $hosts{$database};
        }
        $database = $opts{database} || &Local_CurrentUser();
    }
    return $self->SUPER::SQL_OpenDatabase( $database, %opts );
}
