# Begin-Doc
###############################################################################
# MST::Core:Semester
# Type:         Module
# Description:  functions that operate based on temporal data about school
#               terms (incorrectly called 'semesters')
# Functions:    Semester_GetSchoolTerm
#               Semester_GetTermArray
# Subroutines:  _get_current_date
#               _find_term_by_date
###############################################################################
# End-Doc

###############################################################################
# Background and restrictions:
#
#    The University of Missouri implemented the PeopleSoft Student Database,
#    at the Rolla campus, in January 2004.  The word "semester" is not used
#    in the University's implementation of PeopleSoft; the proper word to
#    use is "term".
#
#    The session term (strm) is always a character string composed of only
#    digits with a length = 4.  The first two digits map to a calendar year.
#    Since January 1996, the last two digits map to a specific strm within
#    that calendar year.  The most important session term codes (as of the
#    March 2013 version of the script) used by the Rolla campus are:
#    27 = Spring, 35 = Summer and 43 = Fall.  Spring and Fall terms are 16
#    weeks; Summer is 8 weeks. For terms since January 1996, adding 197200
#    to the strm value will produce the Rolla internal term (YYYYTT),
#    where YYYY is the calendar year and TT is 27, 35 or 43.
#
#    The Missouri S & T implementation for the value of strm (session term)
#    has these restrictions:
#    1.  Since it is possible that the scheme used for the strm could change
#        at any time, this module may need to be significantly updated
#        *and/or* deprecated, then finally decommissioned.
#    2.  The strm value has an irregular pattern prior to 1996.  Dates prior
#        to December 16, 1995 will fail when used with this module.
#    3.  The strm will not properly work beyond the year 2071
#        (strm = 9927, which represents term Spring 2071).
#    4.  Generally speaking, the pre-1996 terms have some non-PK columns
#        that are inconsistent or unreliable.
#    5.  Terms that are more than about 1-2 years in the future do not have
#        reliable start and end dates. This is the reason why we have to
#        receive permision from the registrar to copy term data into this
#        module at least once each year.
###############################################################################

use strict;

package MST::Core::Semester;

require Exporter;

use vars qw(@ISA @EXPORT);
@ISA = qw(Exporter);
@EXPORT
    = qw(Semester_GetSchoolTerm Semester_CheckCalendar Semester_GetAdjacentTerm Semester_GetTermArray Semester_GetTermInfo);

use Date::Calc qw(check_date Add_Delta_Days Add_Delta_YMD Delta_Days English_Ordinal Month_to_Text);
use Date::Format;
use LWP::Simple;
use File::Path;
use JSON;
use MST::Env;
use MST::UsageLogger;

# This variable is used for passing an error message to the calling program
our $ErrorMsg = undef;

my @SCHOOL_TERMS = ();

# Give names to specific useful dates in the SCHOOL TERMS array
my $last_date_term_zero;
my $first_date_available;
my $last_date_available;

BEGIN {
    &LogAPIUsage();
}

# Begin-Doc
# Name: _init_cachefile
# Type: function
# Syntax: &_init_cachefile
# Returns: directory and filename for cache in list context
# End-Doc
sub _init_cachefile {
    my $cache;
    my $home;
    eval { $home = ( getpwuid($>) )[7]; };

    if ($home) {
        $cache = $home . "/tmp/core-sem-cache";
        mkpath( [$cache], 0, 0700 );
    }
    elsif ($^O =~ /Win32/
        && -e $ENV{TEMP}
        && $ENV{TEMP} =~ m{docum}io )
    {
        $cache = $ENV{TEMP} . "/core-sem-cache";
        mkpath( [$cache], 0, 0700 );
    }

    if ( !-d $cache ) {
        undef($cache);
    }

    return ( $cache, "${cache}/semesters.json" );
}

# Begin-Doc
# Name: _init_data
# Type: function
# Syntax: &_init_data();
# End-Doc
my $core_sem_data;

sub _init_data {
    return if $core_sem_data;

    my $env     = &MST_Env();
    my $rpchost = "mstcoresem.apps.mst.edu";

    if ( $env eq "dev" ) {
        $rpchost = "mstcoresem.apps-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "mstcoresem.apps-test.mst.edu";
    }

    my $location = "https://$rpchost/cgi-bin/cgiwrap/mstcoresem/json-semesters.pl";
    my $sem_text = ();
    my ( $cache, $cachefilename ) = &_init_cachefile;

    if ( defined($cache) ) {

        # Cache for 6 hours
        my $ttl = 6 * 60 * 60;

        # don't try remirroring if we've modified the inode of the cache file in the last 30 seconds
        my @tmpstat = stat($cachefilename);
        unless ( time - $tmpstat[10] < 30 || time - $tmpstat[9] < $ttl ) {
            my $res = mirror( $location, $cachefilename );
        }

        if ( -f $cachefilename ) {
            open( my $tfh, "<$cachefilename" );
            $sem_text = join( "", <$tfh> );
            close($tfh);
        }
    }

    if ( !$sem_text ) {
        $sem_text = get($location);
    }

    eval { $core_sem_data = decode_json($sem_text); };
    if ( !$core_sem_data ) {
        $core_sem_data = { data => [] };
        defined($cache) && unlink($cachefilename);
    }

    @SCHOOL_TERMS = ();
    foreach my $rec ( @{ $core_sem_data->{data} } ) {

        #     [ 'SS2017', 4535, '2017-06-05', '2017-07-28' ]
        push( @SCHOOL_TERMS, [ $rec->{term}, $rec->{strm}, $rec->{date_begin}, $rec->{date_end} ] );
    }

    $last_date_term_zero  = $SCHOOL_TERMS[0][3];     # Example: '1995-12-15'
    $first_date_available = $SCHOOL_TERMS[1][2];     # Example: '1996-01-10'
    $last_date_available  = $SCHOOL_TERMS[-1][3];    # Example: '2014-07-25' as of March 2013
}

# Begin-Doc
###############################################################################
# Name:     Semester_GetSchoolTerm
#
# Type:     Function
#
# Syntax:   Semester_GetSchoolTerm(%options)
#               || die $MST::Core::Semester::ErrorMsg;
#
#           The %options hash contains the keys 'search_date',
#                                               'between_terms', and
#                                               'term_format'
#
#           Each hash pair is optional and the entire hash is optional.
#
# Returned: A code or description of a term (as a scalar value)
#
###############################################################################
# Instructions for using the parameters passed to this sub:
#
#    Passed Parameters:  A hash of named parameters (all optional):
#
#    SEARCH_DATE parameter:
#      If not provided, search date will default to current date.
#      The search date parameter must have the format YYYY-MM-DD.
#      The function will use this parameter to determine which
#      term was/is/will be in session on the search date.
#
#    BETWEEN_TERMS parameter:
#      If not provided, between_terms will default to 'next'.
#      The between_terms parameter determines what to do when the
#      search date falls *BETWEEN* terms.  This parameter is only
#      used when search date falls outside of the range defined by
#      the start and end dates of every term; otherwise, it has no
#      meaning.
#      Valid values and corresponding result are shown:
#      VALUE       RESULTING ACTION
#      ----------  ----------------------------------
#      'next'      the next term will be returned
#      'previous'  the previous term will be returned
#      'ignore'    an empty string will be returned
#      null        the next term will be returned
#
#    TERM_FORMAT parameter:
#      If not provided, term_format will default to 'description'.
#      The term_format parameter determines the format of the
#      returned value.  If term_format is 'legacy', the internal
#      Rolla term code will be returned as six-digits using the
#      Oracle data type VARCHAR2(6).  The format is YYYYTT where
#      YYYY is the calendar year and TT represents the term:
#          27 = Spring, 35 = Summer, 43 = Fall
#      Valid values and corresponding result are shown:
#      VALUE          RESULTING ACTION
#      -------------  ----------------------------------
#      'description'  PS_TERM_TBL.DESCRSHORT will be returned
#      'peoplesoft'   PS_TERM_TBL.STRM will be returned
#      'legacy'       internal Rolla term code will be returned
#      null           PS_TERM_TBL.DESCRSHORT will be returned
#
# IMPORTANT:  It is strongly recommended that no dependencies be
#             built into any script based on the DESCRIPTION value.
#             There is no guarantee that the description will have
#             any kind of predictable format.  Description should
#             only be used for DISPLAY PURPOSES!
#
###############################################################################
# End-Doc
sub Semester_GetSchoolTerm {
    my %arg = @_;

    &_init_data();

    # Reset error message
    $ErrorMsg = undef;

    # Assign args to scalar vars and set defaults for cases where no option is supplied
    my $search_date   = exists $arg{search_date}   ? $arg{search_date}   : _get_current_date();
    my $between_terms = exists $arg{between_terms} ? $arg{between_terms} : 'next';
    my $term_format   = exists $arg{term_format}   ? $arg{term_format}   : 'description';

    # Validate named parameters
    # Since there are three params passed to this sub, each one will have its own error message
    my $ErrorMsg1 = '';
    my $ErrorMsg2 = '';
    my $ErrorMsg3 = '';

    # Define variable that might be needed for a special situation when between terms is 'previous'
    my $date_9_days_earlier = '';

    # First, validate the search date param value
    if ( ( length($search_date) == 10 ) && ( $search_date =~ /^\d{4}-\d{2}-\d{2}$/ ) ) {
        ########## BEGIN ERROR CHECKING OF SEARCH DATE (with a known format of yyyy-mm-dd) ##########
        my ( $year, $month, $day ) = split( '-', $search_date );
        ## The first part of this if-else stream will catch fundamental errors
        if ( ( $month < 1 ) || ( $month > 12 ) ) {
            $ErrorMsg1 = "Invalid search date month $month (must be YYYY-MM-DD): " . $search_date;
        }
        elsif ( ( $day < 1 ) || ( $day > 31 ) ) {
            $ErrorMsg1 = "Invalid search date day $day (must be YYYY-MM-DD): " . $search_date;
        }
        elsif ( ( $day > 28 ) && ( !check_date( $year, $month, $day ) ) ) {
            $ErrorMsg1 = "Invalid search date day $day for the specified month $month: " . $search_date;
        }
        elsif ( $search_date le $last_date_term_zero ) {
            my ( $Yzero, $Mzero, $Dzero ) = split( '-', $last_date_term_zero );
            my $next_day = sprintf( '%04d-%02d-%02d', Add_Delta_YMD( $Yzero, $Mzero, $Dzero, 0, 0, 1 ) );
            my ( $Ynext, $Mnext, $Dnext ) = split( '-', $next_day );
            my $long_date_fmt = sprintf( "%s %s, %d", Month_to_Text($Mnext), English_Ordinal($Dnext), $Ynext );
            $ErrorMsg1 = "This function cannot process dates prior to $long_date_fmt: " . $search_date;
        }
        elsif ( $year > 2071 ) {
            $ErrorMsg1 = "This function cannot process dates after the year 2071: " . $search_date;
        }
        ## The remainder of this if-else stream will catch date boundry errors based on dates in SCHOOL TERMS array
        elsif ( $search_date lt $first_date_available ) {
            if ( $between_terms ne 'next' ) {
                $ErrorMsg1 = "Term information is not available for this date: " . $search_date;
            }
        }
        elsif ( $search_date gt $last_date_available ) {
            my $Emsg = "Term information is not yet available for this date: " . $search_date;
            if ( $between_terms ne 'previous' ) {
                $ErrorMsg1 = $Emsg;
            }
            else {

                # At this point the between terms option is 'previous'; an alternate search date might be possible!
                # There are a minimum of 9 days between terms.  If the search date is no more than 9 days past the
                # end of the last term, then that date (9 days earlier than the search date) will be computed.
                $date_9_days_earlier = sprintf( '%04d-%02d-%02d', Add_Delta_YMD( $year, $month, $day, 0, 0, -9 ) );
                if ( $date_9_days_earlier gt $last_date_available ) {
                    $ErrorMsg1           = "The " . $Emsg;
                    $date_9_days_earlier = '';
                }
            }
        }
    }
    else {
        $ErrorMsg1 = "Invalid search date format. (Must be YYYY-MM-DD): " . substr( $search_date, 0, 30 );
    }

    # Next, validate the between terms param value
    if ( $between_terms !~ /\A(next|previous|ignore)\z/ ) {
        $ErrorMsg2 = "Invalid between terms option: " . substr( $between_terms, 0, 30 );
    }

    # Finally, validate the term format param value
    if ( $term_format !~ /\A(description|peoplesoft|legacy)\z/ ) {
        $ErrorMsg3 = "Invalid term format option: " . substr( $term_format, 0, 30 );
    }

    # If an error was detected, assemble message and return
    if ( $ErrorMsg1 || $ErrorMsg2 || $ErrorMsg3 ) {
        if ($ErrorMsg1) {
            $ErrorMsg = $ErrorMsg1;
        }
        if ($ErrorMsg2) {
            $ErrorMsg .= '; ' if ($ErrorMsg);
            $ErrorMsg .= $ErrorMsg2;
        }
        if ($ErrorMsg3) {
            $ErrorMsg .= '; ' if ($ErrorMsg);
            $ErrorMsg .= $ErrorMsg3;
        }
        return undef;
    }

    # Get information by doing a term lookup by date
    my ($ps_descrshort,     $ps_strm,     $ps_term_begin_dt,     $ps_term_end_dt,
        $prior_descrshort,  $prior_strm,  $prior_term_begin_dt,  $prior_term_end_dt,
        $follow_descrshort, $follow_strm, $follow_term_begin_dt, $follow_term_end_dt
    ) = _find_term_by_date( $search_date, $between_terms, $date_9_days_earlier );

    # If the function call above generated an error, return
    if ($ErrorMsg) {
        return undef;
    }

    # If between terms is 'ignore' and no data was returned for the search date, then done - return
    if ( $between_terms eq 'ignore' ) {
        unless ( $ps_descrshort || $ps_strm || $ps_term_begin_dt || $ps_term_end_dt ) {
            return undef;
        }
    }

    # If the term for the search date was not found, then check between terms param for 'previous' or 'next'
    if ( !$ps_strm ) {
        if ( $between_terms eq 'previous' ) {
            $ps_descrshort    = $prior_descrshort;
            $ps_strm          = $prior_strm;
            $ps_term_begin_dt = $prior_term_begin_dt;
            $ps_term_end_dt   = $prior_term_end_dt;
        }
        elsif ( $between_terms eq 'next' ) {
            $ps_descrshort    = $follow_descrshort;
            $ps_strm          = $follow_strm;
            $ps_term_begin_dt = $follow_term_begin_dt;
            $ps_term_end_dt   = $follow_term_end_dt;
        }
    }

    # There is an error if any of the parts are missing
    if ( !$ps_descrshort || !$ps_strm || !$ps_term_begin_dt || !$ps_term_end_dt ) {
        $ErrorMsg
            = "ERROR: Term data is missing using $search_date / $between_terms (values returned: "
            . substr( $ps_descrshort,    0, 50 ) . ", "
            . substr( $ps_strm,          0, 30 ) . ", "
            . substr( $ps_term_begin_dt, 0, 30 ) . ", "
            . substr( $ps_term_end_dt,   0, 30 ) . ")";
        return undef;
    }

    # Identify errors in the strm returned by the sub _find_term_by_date
    if ( ( length($ps_strm) != 4 ) || ( $ps_strm !~ /^\d{4}$/ ) ) {
        $ErrorMsg = "ERROR: The PS term has an invalid format (value returned: " . substr( $ps_strm, 0, 30 ) . ")";
        return undef;
    }
    elsif ( $ps_strm lt '2427' ) {
        $ErrorMsg = "ERROR: Term $ps_strm is prior to the available terms using $search_date / $between_terms.";
        return undef;
    }
    elsif ( $ps_strm gt '9927' ) {
        $ErrorMsg = "ERROR: Term $ps_strm is after the available terms using $search_date / $between_terms.";
        return undef;
    }
    elsif ( $ps_strm !~ /^\d{2}(27|35|43)$/ ) {
        $ErrorMsg = "ERROR: Term $ps_strm format is invalid using $search_date / $between_terms.";
        return undef;
    }

    # Error, if the returned description is not 4 to 30 characters in length
    my $descr_len = length($ps_descrshort);
    if ( ( $descr_len < 4 ) || ( $descr_len > 30 ) ) {
        $ErrorMsg
            = "ERROR: Term description length is invalid using $search_date / $between_terms. "
            . "(length= $descr_len, value returned: "
            . substr( $ps_descrshort, 0, 50 ) . ").";
        return undef;
    }

    # Compute the value of internal term
    my $term_internal = $ps_strm + 197200;
    if ( ( $term_internal < 199627 ) || ( $term_internal > 207127 ) ) {
        $ErrorMsg = "ERROR: Internal term is invalid using $search_date / $between_terms. (value= $term_internal)";
        return undef;
    }

    #############################
    # Return the requested data #
    #############################
    if ( $term_format eq 'description' ) {
        return $ps_descrshort;
    }
    elsif ( $term_format eq 'peoplesoft' ) {
        return $ps_strm;
    }
    else {
        return $term_internal;
    }
}

# Begin-Doc
# Name: Semester_CheckCalendar
# Description: takes hash of "schedules" and compares "today" against academic calendar (semesters.json cache)
#              and returns which schedules are a match for "today"
# Syntax: my $result = &Semester_CheckCalendar(%options);
# Passed: hash that is defined as follows:
#         "label" => {schedule object or arrayref of schedule objects}
#         NOTE: labels are arbitrary strings that are used in the return and are then handled by application code for logic/code flow
#
#         schedule object defined over the following keys:
#         date_field    - which date field to compare against today (default: date_begin)
#                         valid values:
#                              date_begin
#                              date_end
#                              deadweek_begin
#                              deadweek_end
#                              finals_begin
#                              finals_end
#         offset        - integer value to compare how many days before/after the date field to match with "today"
#                         example offset = 1 (assuming date_field is default) will only match with "today" on the day after the beginning of a semester
#                         (default: 0)
#
#                         can specify a range as "low .. high"
#                         example offset = "0 .. 3" will match on the first three days of a semester
#
#         pattern_field - which field to compare pattern matching against (default: strm) valid values: strm, term
#         pattern       - define regex pattern to only match against certain terms
#                         example: (assuming pattern_field default) pattern = '^\d{2}35$' will only compare summer strms against "today"
#
#         date_cmp      - date comparator - overrides offset behavior with user supplied function. Function will take two arguments:
#                             $calendar_date - the date in the academic calendar
#                             $today         - what "today" is perceived as
#                         the function will return 1 if $today should match with the given $calendar_date, 0 otherwise
#
#         today         - override of what "today" actually is - formatted as YYYY-MM-DD
#
# Returns:
#     hashref, each key is the label that matches the semester's academic calendar based on it's schedule object
#     each corresponding value is an arrayref of each semester that matched with that label,
#     undef on error - check $MST::Core::Semester::ErrorMsg for error
#
# Example:
#   Consider the following code
#
#        sub first_two_weeks {
#            my ( $calendar_date, $today ) = @_;
#
#            my @calendar_date = split( /-/, $calendar_date );
#            my @today         = split( /-/, $today );
#
#            my @first_monday = Monday_of_Week( Week_of_Year(@calendar_date) );
#            my @last_friday = Add_Delta_Days( @first_monday, 11 );
#
#            if ( Delta_Days( @first_monday, @today ) >= 0 && Delta_Days( @last_friday, @today ) <= 0 ) {
#                return 1;
#            }
#            return 0;
#        }
#
#        sub last_two_weeks {
#            my ( $calendar_date, $today ) = @_;
#            my @calendar_date = split( /-/, $calendar_date );
#            my @today         = split( /-/, $today );
#
#            my @first_monday = Add_Delta_Days( Monday_of_Week( Week_of_Year(@calendar_date) ), -7 );
#            my @last_friday = Add_Delta_Days( @first_monday, 11 );
#            if ( Delta_Days( @first_monday, @today ) >= 0 && Delta_Days( @last_friday, @today ) <= 0 ) {
#                return 1;
#            }
#            return 0;
#        }
#
#        my $res = Semester_CheckCalendar(
#            "schedule1" => { offset => 2,  pattern_field => "term",     pattern => '^(SP|FS)\d{4}$', today => "2020-01-23" }, # fire two days after beginning of sp/fs semesters
#            "schedule2" => { offset => -3, date_field    => "date_end", today   => "2019-05-14" },                            # fire three days before the end of a semester
#            "schedule3" => [                                                                                                  # fire every day for the first two and last two weeks of summer semester
#                { date_cmp => \&first_two_weeks, pattern    => '^\d{2}35$' },
#                { date_cmp => \&last_two_weeks,  date_field => "date_end", pattern => '^\d{2}35$' }
#            ]
#        );
#
#        if ( !defined($res) ) {
#            die $MST::Core::Semester::ErrorMsg;
#        }
#        print "today is: ", time2str( "%Y-%m-%d", time ), "\n";
#        print Dumper($res), "\n";
#
#   The routine will take those schedules and compare them agains the semesters.json cache file generated by MST::Core::Semester:
#      ...
#      {
#         "date_begin" : "2019-08-19",
#         "date_end" : "2019-12-13",
#         "deadweek_begin" : "2019-12-02",
#         "deadweek_end" : "2019-12-06",
#         "finals_begin" : "2019-12-09",
#         "finals_end" : "2019-12-13",
#         "strm" : "4743",
#         "term" : "FS2019"
#      },
#      {
#         "date_begin" : "2020-01-21",
#         "date_end" : "2020-05-15",
#         "deadweek_begin" : "2020-05-04",
#         "deadweek_end" : "2020-05-08",
#         "finals_begin" : "2020-05-11",
#         "finals_end" : "2020-05-15",
#         "strm" : "4827",
#         "term" : "SP2020"
#      },
#      ...
#
#   And the following is the output from the above code snippet:
#    today is: 2019-06-13
#    $VAR1 = {
#            'schedule2' => [
#                            {
#                                'finals_begin' => '2019-05-13',
#                                'term' => 'SP2019',
#                                'date_end' => '2019-05-17',
#                                'deadweek_end' => '2019-05-10',
#                                'strm' => '4727',
#                                'date_begin' => '2019-01-22',
#                                'finals_end' => '2019-05-17',
#                                'deadweek_begin' => '2019-05-06'
#                            }
#                            ],
#            'schedule3' => [
#                            {
#                                'term' => 'SS2019',
#                                'finals_begin' => '2019-07-22',
#                                'date_end' => '2019-07-26',
#                                'deadweek_end' => '2019-07-19',
#                                'date_begin' => '2019-06-03',
#                                'finals_end' => '2019-07-26',
#                                'strm' => '4735',
#                                'deadweek_begin' => '2019-07-15'
#                            }
#                            ],
#            'schedule1' => [
#                            {
#                                'date_end' => '2020-05-15',
#                                'deadweek_end' => '2020-05-08',
#                                'term' => 'SP2020',
#                                'finals_begin' => '2020-05-11',
#                                'finals_end' => '2020-05-15',
#                                'date_begin' => '2020-01-21',
#                                'strm' => '4827',
#                                'deadweek_begin' => '2020-05-04'
#                            }
#                            ]
#            };
# End-Doc
sub Semester_CheckCalendar {
    my $arg;
    my %schedule;

    &_init_data;

    #
    # argument parsing/validation
    #
    if ( scalar @_ == 1 ) {
        $arg = shift;

        if ( !-f $arg ) {
            $ErrorMsg = "ERROR: Unknown argument ($arg)";
            return undef;
        }

        my $fd;
        if ( !open( $fd, "<:utf8", $arg ) ) {
            $ErrorMsg = "ERROR: Unable to open config file ($arg)";
            return undef;
        }
        my $json_raw = join( "", <$fd> );
        close($fd);

        my $raw;
        eval { $raw = decode_json($json_raw); };
        if ( !defined $raw ) {
            $ErrorMsg = "ERROR: Unable to parse config file ($arg)";
            return undef;
        }
        %schedule = %{$raw};
    }
    else {
        %schedule = @_;
    }

    #
    # Grab semester information from cache file
    #
    my @semesters;
    {
        my ( undef, $cfile ) = &_init_cachefile;
        if ( !-f $cfile ) {
            $ErrorMsg = "ERROR: Unable to locate cachefile ($cfile)";
            return undef;
        }

        my $fd;
        if ( !open( $fd, "<:utf8", $cfile ) ) {
            $ErrorMsg = "ERROR: Unable to open cachefile ($cfile)";
            return undef;
        }

        my $json_raw = join( "", <$fd> );
        close($fd);

        my $raw;
        eval { $raw = decode_json($json_raw); };

        if ( !defined $raw || !defined $raw->{data} ) {
            $ErrorMsg = "ERROR: Unable to parse cachefile ($cfile)";
            return undef;
        }

        @semesters = @{ $raw->{data} };
    }

    #
    # horribly inefficient O(n x m) search, good thing semesters and academic schedules are highly tractable
    # will consider re-implementing if there is any noticeable performance impact
    #
    my $res       = {};
    my $curr_date = time2str( "%Y-%m-%d", time );

    foreach my $label ( keys %schedule ) {
        my @sub_schedules = ();

        # only singular entry for this label
        if ( ref( $schedule{$label} ) eq "HASH" ) {
            push( @sub_schedules, $schedule{$label} );
        }
        elsif ( ref( $schedule{$label} ) eq "ARRAY" ) {
            @sub_schedules = @{ $schedule{$label} };
        }
        else {
            $ErrorMsg = "ERROR: Unknown schedule spec for label ($label)";
            return undef;
        }

        foreach my $sched (@sub_schedules) {
            my $offset     = $sched->{offset};
            my $pattern    = $sched->{pattern};
            my $pfield     = $sched->{pattern_field} || "strm";
            my $dfield     = $sched->{date_field}    || "date_begin";
            my $comparator = $sched->{date_cmp};
            my $today      = $curr_date;
            my $range_cmp  = 0;

            if ( !defined $offset ) {
                $offset = 0;
            }

            if ( defined $sched->{today} ) {
                $today = $sched->{today};
            }

            my @today = split( /-/, $today );

            my $offset_low;
            my $offset_high;
            if ( $offset =~ /^(-?\d+)\s*\.\.\s*(-?\d+)$/ ) {
                $range_cmp   = 1;
                $offset_low  = $1 + 0;
                $offset_high = $2 + 0;

                if ( $offset_high < $offset_low ) {
                    $ErrorMsg = "ERROR: Range spec for label ($label) out of order ($offset)";
                    return undef;
                }
            }

            foreach my $sem (@semesters) {
                if ( !defined( $sem->{$dfield} ) ) {
                    $ErrorMsg = "ERROR: Unknown date field ($dfield)";
                    return undef;
                }

                if ( !defined( $sem->{$pfield} ) ) {
                    $ErrorMsg = "ERROR: Unknown pattern field ($pfield)";
                    return undef;
                }

                if ( $sem->{$pfield} !~ /$pattern/ ) {

                    # skip if term does not match desired pattern
                    next;
                }

                my $sem_date = $sem->{$dfield};
                my @sem_date = split( /-/, $sem_date );

                # use user-supplied function
                if ($comparator) {
                    my $match = 0;
                    eval { $match = &{$comparator}( $sem_date, $today ); };

                    if ($match) {
                        push( @{ $res->{$label} }, $sem );
                    }
                }
                elsif ($range_cmp) {
                    my @sem_date_low  = Add_Delta_Days( @sem_date, $offset_low );
                    my @sem_date_high = Add_Delta_Days( @sem_date, $offset_high );

                    if ( Delta_Days( @today, @sem_date_low ) <= 0 && Delta_Days( @today, @sem_date_high ) >= 0 ) {

                        # today is between low and high dates of range
                        push( @{ $res->{$label} }, $sem );
                    }
                }
                else {
                    my $offset_date = sprintf( "%.4d-%.2d-%.2d", Add_Delta_Days( @sem_date, $offset ) );
                    if ( $offset_date eq $today ) {
                        push( @{ $res->{$label} }, $sem );

                        # short circuit since singular offset comparisons cannot overlap between semesters
                        last;
                    }
                }
            }
        }
    }
    return $res;
}

# Begin-Doc
# Name: Semester_GetAdjacentTerm
# Type: Function
# Description: Grabs term adjacent to supplied strm, if no strm is supplied - uses "current" strm
# Syntax: my $term = Semester_GetAdjacentTerm(%options);
#   %options defined on keys:
#       strm - grab term adjacent to supplied strm, if no strm is supplied Semester_GetAdjacentTerm will
#           try to guess the current term using search_date and between_terms keys
#           see documentation for Semester_GetSchoolTerm for more information
#       search_date - see documentation for strm
#       between_terms - see documentation for strm
#       format - which format to return results as. Valid values as follows:
#           peoplesoft - (default) ps_term_tbl.strm will be returned
#           description - ps_term_tbl.descrshort will be returned
#           legacy - internal Rolla term code will be returned
#       direction - which adjacent term to grab. Valid values as follows:
#           next - term after supplied strm will be returned
#           previous - term before supplied strm will be returned
# Returns: scalar value on success, undef on error (Check $MST::Core::Semester::ErrorMsg for more information)
# End-Doc
sub Semester_GetAdjacentTerm {
    my %opts = @_;
    &_init_data();

    my $format    = $opts{format} || "peoplesoft";
    my $strm      = $opts{strm};
    my $direction = $opts{direction} || "next";

    if ( $format !~ m/^(peoplesoft|description|legacy)$/ ) {
        $ErrorMsg = "ERROR: Invalid term format (${format})";
        return undef;
    }

    if ( $direction !~ m/^(next|previous)$/ ) {
        $ErrorMsg = "ERROR: Semester_GetAdjacentTerm - invalid direction (${direction}) supplied";
        return undef;
    }

    #
    # If no strm is supplied, grab current strm
    #
    if ( !$strm ) {
        my %currArgs = ();
        my @params   = qw/search_date between_terms/;

        foreach my $param (@params) {
            if ( defined( $opts{$param} ) ) {
                $currArgs{$param} = $opts{$param};
            }
        }
        $currArgs{term_format} = "peoplesoft";
        $strm = &Semester_GetSchoolTerm(%currArgs);

        # Let error message percolate up if there is a failure to retrieve "current" semester
        if ( !$strm ) {
            return undef;
        }
    }

    my $tgt_idx = -1;
    for ( my $idx = 0; $idx < scalar @SCHOOL_TERMS; $idx++ ) {
        my $tgt = $SCHOOL_TERMS[$idx];
        my ( $tgt_term, $tgt_strm, $tgt_begin, $tgt_end ) = @{$tgt};

        if ( $tgt_strm eq $strm ) {
            if ( $direction eq "next" && exists $SCHOOL_TERMS[ $idx + 1 ] ) {
                $tgt_idx = $idx + 1;
                last;
            }
            elsif ( $direction eq "previous" && exists $SCHOOL_TERMS[ $idx - 1 ] ) {
                $tgt_idx = $idx - 1;
                last;
            }
        }
    }

    if ( $tgt_idx == -1 ) {
        $ErrorMsg = "ERROR: Unable to locate term adjacent to ${strm} (${direction})";
        return undef;
    }

    my $target = $SCHOOL_TERMS[$tgt_idx];
    my ( $tgt_term, $tgt_strm, $tgt_begin, $tgt_end ) = @{$target};

    if ( $format eq "peoplesoft" ) {
        return $tgt_strm;
    }
    elsif ( $format eq "description" ) {
        return $tgt_term;
    }
    else {

        # legacy formatting
        return $tgt_strm + 197200;
    }
}

# Begin-Doc
###############################################################################
# Name: Semester_GetTermArray
# Type: Function
#
# Syntax: Semester_GetTermArray()
#
# Description: The SCHOOL TERMS array, as defined in this perl
#     module, is returned to the calling script.
#
# Passed: None
#
# Returned: Array of school terms
#
###############################################################################
# End-Doc

sub Semester_GetTermArray {
    &_init_data();

    return @SCHOOL_TERMS;
}

# Begin-Doc
###############################################################################
# Name: Semester_GetTermInfo
# Type: Function
#
# Syntax: $info = Semester_GetTermInfo($term)
#
# Description: Returns ref to hash of info for terms, keys are 'date_begin', 'date_end', 'strm',
# 'term', 'finals_begin', 'finals_end', 'deadweek_begin', 'deadweek_end'
# Can be called with either strm or term value
#
###############################################################################
# End-Doc

sub Semester_GetTermInfo {
    my $term = shift @_;

    &_init_data();

    my $iref = {};
    foreach my $rec ( @{ $core_sem_data->{data} } ) {
        if ( $rec->{term} eq $term || $rec->{strm} eq $term ) {
            $iref = $rec;
            last;
        }
    }

    return $iref;
}

# Begin-Doc
###############################################################################
# Name: _get_current_date
# Type: Function
#
# Syntax: _get_current_date()
#
# Description: Get the current date in YYYY-MM-DD format
#
# Passed: None
#
# Returned: The current date
#
###############################################################################
# End-Doc

sub _get_current_date {
    my ( $day, $month_offset, $year_offset ) = (localtime)[ 3, 4, 5 ];

    return sprintf( '%04d-%02d-%02d', $year_offset + 1900, $month_offset + 1, $day );
}

# Begin-Doc
###############################################################################
# Name: _find_term_by_date
# Type: Function
#
# Syntax: _find_term_by_date($search_date, $between_terms, $date_9_days_earlier)
#
# Description: Searches for data associated with a date in the
#              school terms array
#
# Passed: Search date
#
# Returned: Three arrays of data for up to three terms.  The array
#           fields are description, peoplesoft code, begin date,
#           and end date.  The first array holds data for the
#           target term.  The second array holds data for the
#           term previous to the target term.  The third array holds
#           data for the term following to the target term.  If any
#           of these terms fall outside of @SCHOOL_TERMS dates, the
#           fields will be nulls.
###############################################################################
# End-Doc

sub _find_term_by_date {

    &_init_data();

    my ( $search_date, $between_terms, $date_9_days_earlier ) = @_;

    # Set up the return variables
    my @term_cr_data = ( '', '', '', '' );    # Term data for the current term
    my @term_pr_data = ( '', '', '', '' );    # Term data for the term prior to the current term
    my @term_fl_data = ( '', '', '', '' );    # Term data for the term following the current term

    if ($ErrorMsg) {
        return ( @term_cr_data, @term_pr_data, @term_fl_data );
    }

    # Reset error message
    $ErrorMsg = undef;

    # Basic validation of the search date param
    if ( $search_date =~ /^\d{4}-\d{2}-\d{2}$/ ) {
        my ( $sd_yr, $sd_mo, $sd_da ) = split( '-', $search_date );
        if ( !check_date( $sd_yr, $sd_mo, $sd_da ) ) {
            $ErrorMsg = "ERROR_find_term_by_date. Invalid search date: " . $search_date;
        }
    }
    else {
        $ErrorMsg = "ERROR_find_term_by_date. Invalid search date format. (Must be YYYY-MM-DD): "
            . substr( $search_date, 0, 30 );
    }
    if ($ErrorMsg) {
        return ( @term_cr_data, @term_pr_data, @term_fl_data );
    }
    ##### end Basic validation #####

    # Search date validation (date is too old): examine param to identify dates that are too far into the past
    if ( $search_date lt $first_date_available ) {
        if ( $between_terms eq 'next' ) {
            if ( $search_date le $last_date_term_zero ) {
                $ErrorMsg = "ERROR_find_term_by_date. The search date is too far in the past: " . $search_date;
            }
        }
        else {
            $ErrorMsg = "ERROR_find_term_by_date. Search date is too far in the past: " . $search_date;
        }
    }
    if ($ErrorMsg) {
        return ( @term_cr_data, @term_pr_data, @term_fl_data );
    }
    ##### end Search date validation (date is too old) #####

    # Var to track the validity of the param date_9_days_earlier, which is used for a particular case
    my $param_9_days_earlier_valid = 0;    # Initially, set this flag to indicate the param is invalid

    # Search date validation (date is too new): examine param to identify dates that are too far into the future
    if ( $search_date gt $last_date_available ) {

        # If between terms is 'previous', then Continuing with script *might* be allowed
        if ( $between_terms eq 'previous' ) {

            # Validate the param date_9_days_earlier (to make sure it isn't past the last date available)
            # The value of the param date_9_days_earlier was pre-computed by the calling routine
            if ( $date_9_days_earlier =~ /^\d{4}-\d{2}-\d{2}$/ ) {
                my ( $y9, $m9, $d9 ) = split( '-', $date_9_days_earlier );
                if ( check_date( $y9, $m9, $d9 ) ) {
                    if ( $date_9_days_earlier le $last_date_available ) {
                        $param_9_days_earlier_valid = 1;
                    }
                }
            }
            if ( !$param_9_days_earlier_valid ) {
                $ErrorMsg = "ERROR_find_term_by_date. The search date is too far in the future: " . $search_date;
            }
        }

        # Between terms is NOT 'previous'; there is an error
        else {
            $ErrorMsg = "ERROR_find_term_by_date. Search date is too far in the future: " . $search_date;
        }
    }
    if ($ErrorMsg) {
        return ( @term_cr_data, @term_pr_data, @term_fl_data );
    }
    ##### end Search date validation (date is too new) #####

    # Now that all of this checking has been done, the search date will index to valid data in the SCHOOL TERMS array

    # To make the foreach loop much less complex, two special cases will be handled first...

    # Handle special case: the search date is before the first date available
    if ( $search_date lt $first_date_available ) {
        if ( ( $between_terms eq 'next' ) && ( $search_date gt $last_date_term_zero ) ) {
            @term_fl_data = ( $SCHOOL_TERMS[1][0], $SCHOOL_TERMS[1][1], $SCHOOL_TERMS[1][2], $SCHOOL_TERMS[1][3] );
            return ( @term_cr_data, @term_pr_data, @term_fl_data );
        }
        else {

            # Just in case; however, the logic should never lead here
            $ErrorMsg = "ERROR_find_term_by_date. UNEXPECTED date is too far in the past: " . $search_date;
            return ( @term_cr_data, @term_pr_data, @term_fl_data );
        }
    }

    # Handle special case: the search date is after the last date available
    if ( $search_date gt $last_date_available ) {
        if (   ( $between_terms eq 'previous' )
            && ($param_9_days_earlier_valid)
            && ( $date_9_days_earlier le $last_date_available ) )
        {
            @term_pr_data = ( $SCHOOL_TERMS[-1][0], $SCHOOL_TERMS[-1][1], $SCHOOL_TERMS[-1][2], $SCHOOL_TERMS[-1][3] );
            return ( @term_cr_data, @term_pr_data, @term_fl_data );
        }
        else {

            # Just in case; however, the logic should never lead here
            $ErrorMsg = "ERROR_find_term_by_date. UNEXPECTED date is too far in the future: " . $search_date;
            return ( @term_cr_data, @term_pr_data, @term_fl_data );
        }
    }

    # Set up for the foreach loop
    # Process each processing-term in the array ($array_ref), unless or until last is executed
    my $term_found_status = 0;    # 0 = target term not found, 1 = found target term
    foreach my $array_ref (@SCHOOL_TERMS) {

        # Check to see if ready to leave foreach loop (because Target Term has already been identified)
        if ( $term_found_status == 1 ) {

            # note:  be certain that the data is within boundries
            if ( ( $first_date_available le $array_ref->[2] ) && ( $last_date_available ge $array_ref->[3] ) ) {
                @term_fl_data = @$array_ref;
            }
            last;
        }
        else {

            # Check to see if Target Term is identified
            if ( ( $search_date ge $array_ref->[2] ) && ( $search_date le $array_ref->[3] ) ) {

                # note:  be certain that the data is within boundries
                if ( ( $first_date_available le $array_ref->[2] ) && ( $last_date_available ge $array_ref->[3] ) ) {
                    @term_cr_data      = @$array_ref;
                    $term_found_status = 1;
                }
            }
            else {

                # Check to see if Between Terms condition is identified
                if ( $search_date lt $array_ref->[2] ) {

                    # note:  be certain that the data is within boundries
                    if ( ( $first_date_available le $array_ref->[2] ) && ( $last_date_available ge $array_ref->[3] ) ) {
                        @term_fl_data = @$array_ref;
                    }
                    last;
                }

                # Otherwise, the data currently being processed is for Prior Term
                else {

                    # note:  be certain that the data is within boundries
                    if ( ( $first_date_available le $array_ref->[2] ) && ( $last_date_available ge $array_ref->[3] ) ) {
                        @term_pr_data = @$array_ref;
                    }
                }
            }
        }
    }

    return ( @term_cr_data, @term_pr_data, @term_fl_data );
}

1;
