
=begin

Begin-Doc
Name: MST::UsageLogger
Type: module
Description: Track usage of APIs and scripts

Sends a udp message to a logging service for tracking in a database.

End-Doc

=cut

package MST::UsageLogger;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw(
    LogAPIUsage
    ResetAPIUsage
);

use IO::Socket::INET;
use Sys::Hostname;
use Cwd;

#
# When this module is loaded, preconfigure the socket
#

# Cache some data that we don't check for changes
our %canon_path_cache;
our $usage_logger_host;
our $usage_logger_user;
our $usage_logger_script;
our $usage_logger_scriptowner;

our $usage_logger_type;

our $usage_logger_scriptinvoked = 0;

our $usage_cache;    # only report particular backtrace to server once per script invocation

our $usage_socket = &OpenUsageSocket();

our $last_socket_pid;

# Keep this in sync with apiusage-feed.srv.mst.edu IP. Hardwired to avoid extra dns lookup dependency in all apps
our $server_ip = '131.151.249.129';

# Begin-Doc
# Name: ResetAPIUsage
# Type: subroutine
# Description: resets state of usage logger, intended for persistent scripts to tell this api "clear any cache of 'already run'"
# Syntax: &ResetAPIUsage();
# End-Doc
sub ResetAPIUsage {
    $usage_logger_scriptinvoked = 0;
    $usage_cache                = {};
}

# Begin-Doc
# Name: OpenUsageSocket
# Type: subroutine
# Description: open internal udp socket used by usageLogger routines
# Access: internal
# Syntax: &OpenUsageSocket();
# End-Doc
sub OpenUsageSocket {
    my $sock = IO::Socket::INET->new(
        PeerAddr => $server_ip,
        PeerPort => '2407',
        Proto    => 'udp'
    );

    $last_socket_pid = $$;

    return $sock;
}

# Begin-Doc
# Name: LogAPIUsage
# Type: subroutine
# Description: track api usage
# Access: public
# Syntax: &LogAPIUsage($msg)
#	Optional $msg will be recorded along with usage tracking
# Comments: Should be called from anywhere you want to track usage
# End-Doc
sub LogAPIUsage {
    my ($msg) = @_;
    my ( $user, $script, $scriptowner, $cwd, $authuser, $server );

    # Allow an application to request that we not log anything, useful if it's something executed way too often?
    if ($MST::UsageLogger::IGNORE) {
        return;
    }

    if ( !$usage_logger_script ) {
        $usage_logger_script = &_canon_path($0);
        if (   $script
            && $script =~ m#^/local/(.*?)/# )
        {
            $usage_logger_scriptowner = $1;
        }
    }
    $script      = $usage_logger_script;
    $scriptowner = $usage_logger_scriptowner;

    # This might not be correct for sysadmin type scripts that change uid, but for everyone else, it will be faster
    if ( !$usage_logger_user ) {
        eval { $usage_logger_user = ( getpwuid($<) )[0]; };
    }
    $user = $usage_logger_user;
    if ( !$user ) {
        $user = $scriptowner;
    }

    #
    # This stuff can change frequently
    #
    $cwd = &_canon_path(getcwd);

    if ( $ENV{"REMOTE_USER"} && $ENV{"REMOTE_USER"} ne "" ) {
        $authuser = $ENV{REMOTE_USER};
    }

    #
    # These do not change often
    #
    if ( defined( $ENV{HTTP_HOST} ) && $ENV{"HTTP_HOST"} ne "" ) {
        $server = $ENV{HTTP_HOST};
    }
    else {
        if ( !defined($usage_logger_host) ) {
            $usage_logger_host = hostname;
        }
        $server = $usage_logger_host;
    }

    if ( !defined($usage_logger_type) ) {
        if ( exists( $ENV{MOD_PERL} ) ) {
            $usage_logger_type = "mod_perl";
        }
        elsif ( $ENV{REQUEST_METHOD} ) {
            $usage_logger_type = "cgi";
        }
        elsif ( $ENV{PS1} || $ENV{LS_COLORS} ) {
            $usage_logger_type = "shell";
        }
        else {
            $usage_logger_type = "unknown";
        }
    }

    if ( !$usage_logger_scriptinvoked ) {
        &_SendUsagePacket(
            msg         => "Script Invoked",
            user        => $user,
            script      => $script,
            scriptowner => $scriptowner,
            cwd         => $cwd,
            authuser    => $authuser,
            server      => $server,
            language    => "perl",
            type        => $usage_logger_type,
        );

        # Only send one time per execution
        $usage_logger_scriptinvoked = 1;
    }

    &_SendUsagePacket(
        msg         => $msg,
        user        => $user,
        script      => $script,
        scriptowner => $scriptowner,
        cwd         => $cwd,
        authuser    => $authuser,
        server      => $server,
        language    => "perl",
        type        => $usage_logger_type,
    );

    return undef;
}

sub _canon_path {
    my $file = $_[0];

    if ( !$file ) {
        return undef;
    }

    if ( $file && $canon_path_cache{$file} ) {
        return $canon_path_cache{$file};
    }

    $canon_path_cache{$file} = $file;
    return $file;
}

sub _SendUsagePacket {
    my (%data) = @_;
    my $msg;
    my $res;
    my $dummy;

    if ( $last_socket_pid != $$ ) {
        $usage_socket = &OpenUsageSocket();
    }

    if ($usage_socket) {
        my @tmp;

        while ( my ( $key, $val ) = each %data ) {

            # fast url encoding method
            if ($key) {
                $key =~ s/([^a-zA-Z0-9_\-.])/uc sprintf("%%%02x",ord($1))/eg;
            }
            if ($val) {
                $val =~ s/([^a-zA-Z0-9_\-.])/uc sprintf("%%%02x",ord($1))/eg;
            }

            if ( $key && $val ) {
                push( @tmp, $key . "=" . $val );
            }
        }
        $msg = join( "&", @tmp );

        # in case socket was closed...
        eval { $res = $usage_socket->send($msg); };

        if ( $@ =~ /determine peer address/o ) {
            $usage_socket = IO::Socket::INET->new(
                PeerAddr => $server_ip,
                PeerPort => '2407',
                Proto    => 'udp'
            );
            eval { $res = $usage_socket->send($msg); };
        }
    }

    return 0;
}

1;

