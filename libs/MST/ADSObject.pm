
=begin

Begin-Doc
Name: MST::ADSObject
Type: module
Description: child class around Local::ADSObject to set defaults

End-Doc

=cut

package MST::ADSObject;
require 5.000;
use Exporter;
use strict;
use Local::ADSObject;
use MST::Env;
use MST::UsageLogger;
use Net::DNS;

use vars qw (@ISA @EXPORT);
@ISA    = qw(Local::ADSObject Exporter);
@EXPORT = qw();

# Map to super class
my $ErrorMsg;
*MST::ADSObject::ErrorMsg = *Local::ADSObject::ErrorMsg;

=begin
Begin-Doc
Name: new
Type: method
Description: creates object
Syntax: $obj->new(%params)
Comments: Same syntax/behavior as routine in ADSObject module.
End-Doc
=cut

sub new {
    my $self = shift;
    my @args = @_;
    my %opts = @_;

    # Pre-insert the mappings to local load balancers to avoid dns lookups
    &Local::ADSObject::_preload_domain_server(
        "mst.edu"           => "umad-gc.mst.edu",
        "umad.umsystem.edu" => "umad-gc.mst.edu",
    );

    my %base_opts = ( domain => "umad.umsystem.edu", auth_domain => "umsystem.edu" );

    return $self->SUPER::new( %base_opts, @_ );
}

1;
