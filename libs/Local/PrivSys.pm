
=begin

Begin-Doc
Name: Local::PrivSys
Type: module
Description: Stub set of routines wrapping around MST::PrivSys
End-Doc

=cut

package Local::PrivSys;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

use MST::PrivSys;

@ISA    = qw(Exporter);
@EXPORT = ();

sub import {
    no strict 'refs';
    my $caller = caller;

    while ( my ( $name, $symbol ) = each %MST::PrivSys:: ) {
        next if ( $name !~ /^PrivSys_/ );

        my $imported = $caller . '::' . $name;
        *{$imported} = \*{$symbol};
    }
}

1;
